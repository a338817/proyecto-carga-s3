const { NextApiRequest, NextApiResponse } = require("next");
const S3 = require("aws-sdk/clients/s3");
const { randomUUID } = require("crypto");

const s3 = new S3({
apiVersion: "2006-03-01",
accessKeyId: process.env.ACCESS_KEY,
secretAccessKey: process.env.SECRET_KEY,
region: process.env.REGION,
signatureVersion: "v4",
});

async function handler(req, res) {
    const ex = req.query.fileType.split("/")[1];

    const Key = `${randomUUID()}.${ex}`;

    const s3Params = {
    Bucket: process.env.BUCKET_NAME,
    Key,
    Expires: 60,
    ContentType: `image/${ex}`,
    };

    const uploadUrl = await s3.getSignedUrl("putObject", s3Params);

    console.log("uploadUrl", uploadUrl);

    res.status(200).json({
    uploadUrl,
    key: Key,
    });
}


module.exports = handler;