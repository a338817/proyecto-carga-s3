This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Primero se deben instalar las dependencias con el comando:
```
npm run dev
# o
yarn
```
Para correr el servidor ejecutamos:
```
npm run dev
# or
yarn dev
# or
pnpm dev
```

Abrimos [http://localhost:3000](http://localhost:3000) con el navegador.

Una vez en la aplicaciíon podremos observar la siguiente pantalla

![Pantalla principal](./public/Pruebas/inicio.png)

## ¿Cómo se usa?

- Pimero cargamos nuestro archivo presionando el boton de selecciónar archivos o simplemente arrastrandolos dentro del drag and drop.
![Selección](./public/Pruebas/Seleccion.png)
- Despues presionamos el boton de subir y esperamos.
![Subir](./public/Pruebas/Subir.png)
- Y listo, podemos continuar subiendo archivos.
![Inicio](./public/Pruebas/inicio.png)

## Funcionamiento

El programa de carga a nuestro S3 funciona usando la api creada en la carpeta api llamada media, en este archivo solicitamos a S3 una url para nuestro archivo y despues desde el index utilizamos esta url para realizar una llamada a aws y subir el archivo en cuestion.
