import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import React, {useState} from 'react'
import { IconPresentationAnalytics, IconFileSpreadsheet } from "@tabler/icons-react";
import axios from 'axios'
import { BeatLoader } from 'react-spinners'

export default function Home() {

  const [dragging, setDragging] = useState(false);
  const [files, setFiles] = useState([]);
  const [cargando, setCargando] = useState(false)
  const [carga, setCarga] = useState(0);
  const [cargado, setCargado] = useState(false);

  const handleDragOver = (e) => {
    e.preventDefault();
    setDragging(true);
  };

  const handleDragLeave = (e) => {
    e.preventDefault();
    setDragging(false);
  };

  const handleDrop = (e) => {
    e.preventDefault();
    setDragging(false);

    const newFiles = [...files];

    for (let i = 0; i < e.dataTransfer.files.length; i++) {
      const file = e.dataTransfer.files[i];
      newFiles.push(file);
    }

    setFiles(newFiles);
  };

  function handleFileSelect(event) {
    const filesinput = event.target.files;
    const filesArray = Array.from(filesinput);
    setFiles(filesArray);
  }

  const subirArchivos = async() => {
    setCargando(true);
    
    await files.forEach(async file => {
      const fileType = encodeURIComponent(file.type);
      const { data } = await axios.get(`/api/media?fileType=${fileType}`);
      console.log(data);
      const { uploadUrl, key } = data;

      await axios.put(uploadUrl, file);
    })
    setFiles([]);
    setCargando(false);
    
    setCargado(true);
    setTimeout(() => {
      setCargado(false)
    }, 2000);
  }



  return (
    <div className='flex justify-center h-screen items-center'>
      <div className='border border-sky-50 w-1/2 h-3/4 rounded-md'>
        <div className='w-full text-center px-5 pt-5 text-xl font-bold'>
          <h3>Carga un archivo a S3</h3>
        </div>
        <div className='px-10 text-center'>
          <p>Carga aqui tu archivo directo a nuestro bucket de S3</p>
        </div>
        <div class="py-2">
          <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
          <div
            onDragOver={handleDragOver}
            onDragLeave={handleDragLeave}
            onDrop={handleDrop}
            className={`flex flex-col items-center py-12 px-6 rounded-md border-2 border-dashed ${dragging ? 'border-indigo-500' : 'border-gray-400'}`}
          >
              <svg
                class="w-12 h-12 text-gray-500"
                aria-hidden="true" fill="none" stroke="currentColor"
                viewBox="0 0 48 48">
              <path
                  d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                  stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
              </svg>
    
              <p class="text-xl text-gray-700">Drop files to upload</p>
    
              <p class="mb-2 text-gray-700">or</p>
    
              <label class="bg-white px-4 h-9 inline-flex items-center rounded border border-gray-300 shadow-sm text-sm font-medium text-gray-700 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                Select files
                <input type="file" name="file" multiple class="sr-only" onChange={handleFileSelect}/>
              </label>
    
              <p class="text-xs text-gray-600 mt-4">Maximum upload file size: 512MB.</p>
          </div>
        
        </div>
        <ul>
          {files.map((file, index) => (
            <li key={index} className='flex px-12 py-2'>
              <div className='flex items-center mr-2'>
               {file.name.split('.')[1] == 'pptx' ? (
                <IconPresentationAnalytics size={16}/>
               ):(<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark" viewBox="0 0 16 16"> <path d="M14 4.5V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h5.5L14 4.5zm-3 0A1.5 1.5 0 0 1 9.5 3V1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5h-2z"/> </svg>)}
              </div>
              <div className='w-2/3 truncate'>
                {file.name}
              </div>
            </li>
          ))}
        </ul>
        {cargando ? (
          <div className='px-5'>
            <div style={{width:`${carga}%`}} className='bg-gray-400 text-center p-1 rounded-lg overflow-hidden'>
              <p>{`${carga}%`}</p>
            </div>
          </div>
        ): ''}
        {files.length !== 0 ? (<div className='w-full flex justify-center mt-2'>
          <button disabled={cargando} onClick={() => subirArchivos()} className='flex items-center absolute border py-2 px-5 rounded-xl text-white rounded-lg hover:bg-white hover:text-black transition duration-30'>
          {!cargando ? (
            `Subir`
          ): (<>
          <BeatLoader color="#fff" size={6} />
          Procesando</>
          )}
          </button>
        </div>) : ''}
        {
          cargado ?

          (<div className='p-5 flex text-center w-full justify-center'>
          <p className='font-xl font-bold'>Texto cargado!!!</p>
        </div>): ''
        }
      </div>
      </div>
    </div>
  )
}
